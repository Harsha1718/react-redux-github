import { FETCH_COMMENTS, ADD_COMMENT, DELETE_COMMENT ,LOGIN} from './types'
import { Auth, logOut } from '../Authentication'


export const fetchComments = (val) => async (dispatch) => {

    let data = '', comments = '', owner = '';
    await fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${val}`)
        .then(val => val.json())
        .then(json => { data = json })
        .catch((error) => {
            alert('There has been a problem with your fetch operation: mount ', error.message);
        });
    await fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${val}/comments`)
        .then(val => val.json())
        .then(json => { comments = json })
        .catch((error) => {
            alert('There has been a problem with your fetch operation: comments display', error.message);
        });
    await fetch(`https://api.github.com/repos/thousif7/test-issues`)
        .then(val => val.json())
        .then(json => { owner = json })
        .catch((error) => {
            alert('There has been a problem with your fetch operation: comments display', error.message);
        });
    dispatch({
        type: FETCH_COMMENTS,
        payload: { data, owner, comments }
    })
}

export const addComment = (comment, id, token) => dispatch => {
    fetch(`https://api.github.com/repos/thousif7/test-issues/issues/${id}/comments?access_token=${token}`,
        { method: 'POST', body: JSON.stringify({ body: comment }) })
        .then(comment => comment.json())
        .then(json => dispatch({
            type: ADD_COMMENT,
            payload: json
        }))
}

export const deleteComment = (id, token) => dispatch => {
    fetch(`https://api.github.com/repos/thousif7/test-issues/issues/comments/${id}?access_token=${token}`, { method: 'DELETE' })
    dispatch({
        type: DELETE_COMMENT,
        payload: id
    })
}

export const login = (status) => async dispatch => {
    if (status === 'LOG IN') { if (await Auth()) dispatch({ type: LOGIN, payload: "LOG OUT" }) }
    else { logOut(); dispatch({ type: LOGIN, payload: "LOG IN" }) }
}