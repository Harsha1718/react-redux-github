import { FETCH_ISSUES, FILTER_AUTHOR, FILTER_LABEL, FILTER_STATE, TITLE_SEARCH, ISSUES_SORT ,LOGIN} from './types'
import {Auth,logOut} from '../Authentication'

export const fetchIssues = () => dispatch => {
    fetch(`https://api.github.com/repos/thousif7/test-issues/issues?state=all&per_page=100`)
        .then(res => res.json())
        .then(json => dispatch({
            type: FETCH_ISSUES,
            payload: json
        }))
        .catch((error) => { alert('There has been a problem with your fetch operation: first issues', error.message); });
}
export const filterAuthor = (author) => dispatch => {
    dispatch({
        type: FILTER_AUTHOR,
        payload: author
    })
}

export const filterLabel = (label) => dispatch => {
    dispatch({
        type: FILTER_LABEL,
        payload: label
    })
}
export const filterState = (check) =>dispatch => {
    dispatch({
        type: FILTER_STATE,
        payload: check 
    })
}

export const titleSearch = (title) => dispatch => {
    dispatch({
        type: TITLE_SEARCH,
        payload: title
    })
}

export const sortOptions = (option) => dispatch => {
    dispatch({
        type: ISSUES_SORT,
        payload: option
    })
}

export const login = (status) =>async dispatch =>{
    if (status === 'LOG IN') { if(await Auth())dispatch({type: LOGIN,payload: "LOG OUT"})}
    else {logOut();dispatch({type: LOGIN,payload: "LOG IN"})}
}

