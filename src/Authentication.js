const firebase = require('firebase')
let access_token = null || sessionStorage.getItem('access_token');
let userName = null || sessionStorage.getItem('userName');
let appInitiate = false;
const Auth = async () => {
    let bool = true;
    var config = {
        apiKey: "AIzaSyC6SYm3BL7gVajVCFrWeXiEgYxd1ceFWog",
        authDomain: "gthub-issues-firebase.firebaseapp.com",
        databaseURL: "https://gthub-issues-firebase.firebaseio.com",
        projectId: "gthub-issues-firebase",
        storageBucket: "gthub-issues-firebase.appspot.com",
        messagingSenderId: "839987772994"
    };
    if (!appInitiate) {
        appInitiate = true;
        firebase.initializeApp(config);
    }
    var provider = new firebase.auth.GithubAuthProvider();
    provider.addScope('repo');
    await firebase.auth().signInWithPopup(provider).then(function (result) {
        sessionStorage.setItem("access_token", result.credential.accessToken);
        sessionStorage.setItem("userName", result.additionalUserInfo.username);
        access_token = result.credential.accessToken;
        userName = result.additionalUserInfo.username;
        console.log("logged in")
    }).catch(function (error) {
        bool = false;
        alert("authentication error")
    });
    return bool;
}

const logOut = () => {
    sessionStorage.clear();
    access_token = null;
    userName = null;
    console.log("logged out")
    if (appInitiate) {
        firebase.auth().signOut().then(function () {
        }).catch(function (error) {
        });
    }
}

export { Auth, access_token, userName, logOut }