import {combineReducers} from 'redux'

import issueReducer from './issueReducer'
import commentsReducer from './commentsReducer'

export default combineReducers({
    issues:issueReducer,
    comments:commentsReducer
})