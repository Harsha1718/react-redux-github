import { FETCH_COMMENTS, ADD_COMMENT, DELETE_COMMENT ,LOGIN} from '../actions/types'

const initialState = {
    titleData: '',
    comments: [],
    owner: '',
    isLogin:false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS:
            return {
                ...state,
                titleData:action.payload.data,
                comments: action.payload.comments,
                owner:action.payload.owner.owner.login
            }
        case ADD_COMMENT:
            let newData = state.comments.reduce((acc,ele) => { acc.push(ele);return acc; }, [])
            newData.push(action.payload);
            return {
                ...state,
                comments:newData
            }
        case DELETE_COMMENT:
        return {
            ...state,
            comments: state.comments.filter(ele => parseInt(ele.id) !== parseInt(action.payload))
        }
        case LOGIN:
            let  copy = Object.assign([], state.comments);
            return {
                ...state,
                isLogin:(state.isLogin)?false:true,
                comments:copy
            }
        default:
            return {
                ...state,
            }
    }
}