import { FETCH_ISSUES, FILTER_AUTHOR, FILTER_LABEL, FILTER_STATE, TITLE_SEARCH, ISSUES_SORT ,LOGIN} from '../actions/types'
import Fuse from "fuse.js"
let access_token = sessionStorage.getItem("access_token");


const initialState = {
    issueData: [],
    passData: [],
    label: '',
    author: '',
    open: '',
    closed: '',
    login: access_token===null?"LOG IN":"LOG OUT",
    page:1
}

const labelSearch = (data, label) => {
    const constraintData = data.reduce((acc, ele) => {
        const result = ele.labels.filter((labels) => labels.name === label)
        if (result.length) acc.push(ele)
        return acc;
    }, []);
    return constraintData;
}

const authorSearch = (data, author) =>{
    const constraintData  = data.filter((ele) => ele.user.login === author)
    return constraintData;
}


export default (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ISSUES:
            return {
                ...state,
                issueData: action.payload,
                passData: action.payload,
                open: (state.open === '') ? action.payload.filter(ele => ele.state === 'open').length:state.open,
                closed: (state.closed === '') ? action.payload.filter(ele => ele.state === 'closed').length : state.closed,
                // page:action.payload.val
            }

        case FILTER_AUTHOR:
            if (action.payload !== "") {
                let prevData = (state.label !== '') ? labelSearch(state.issueData,state.label) : state.issueData;
                const constraintData = prevData.filter(ele => ele.user.login === action.payload)
                if (constraintData.length) {
                    const openIssues = constraintData.filter(ele => ele.state === 'open');
                    const closedIssues = constraintData.filter(ele => ele.state === 'closed');
                    return {
                        ...state,
                        passData: constraintData,
                        open: openIssues.length,
                        closed: closedIssues.length,
                        author: action.payload
                    }
                }
            }
            else if (action.payload === "") {
                if (state.label !== '') {
                    const constraintData = labelSearch(state.issueData, state.label);
                    const openIssues = constraintData.filter(ele => ele.state === 'open');
                    const closedIssues = constraintData.filter(ele => ele.state === 'closed');
                    return {
                        ...state,
                        author: '',
                        open: openIssues.length,
                        closed: closedIssues,
                        passData: constraintData,
                    }
                }
                else {
                    const openIssues = state.issueData.filter(ele => ele.state === 'open');
                    const closedIssues = state.issueData.filter(ele => ele.state === 'closed');
                    return {
                        ...state,
                        passData: state.issueData,
                        open: openIssues.length,
                        closed: closedIssues.length,
                        author: ''
                    }
                }
            } break;

        case FILTER_LABEL:
            if (action.payload !== "") {
                let prevData = (state.author !== '') ? authorSearch(state.issueData,state.author) : state.issueData;
                const constraintData = prevData.reduce((acc, ele) => {
                    const result = ele.labels.filter((a) => a.name === action.payload)
                    if (result.length) acc.push(ele)
                    return acc;
                }, []);
                if (constraintData.length) {
                    const openIssues = constraintData.filter(ele => ele.state === 'open');
                    const closedIssues = constraintData.filter(ele => ele.state === 'closed');
                    return {
                        ...state,
                        passData: constraintData,
                        open: openIssues.length,
                        closed: closedIssues.length,
                        label: action.payload
                    }
                }
            }
            else if (action.payload === "") {
                if (state.author !== '') {
                    const constraintData =authorSearch(state.issueData,state.author)
                    const openIssues = constraintData.filter(ele => ele.state === 'open');
                    const closedIssues = constraintData.filter(ele => ele.state === 'closed');
                    return {
                        ...state,
                        label: '',
                        open: openIssues.length,
                        closed: closedIssues.length,
                        passData: constraintData,
                    }
                }
                else {
                    const openIssues = state.issueData.filter(ele => ele.state === 'open');
                    const closedIssues = state.issueData.filter(ele => ele.state === 'closed');
                    return {
                        ...state,
                        passData: state.issueData,
                        open: openIssues.length,
                        closed: closedIssues.length,
                        label: ''
                    }
                }
            } break;

        case FILTER_STATE:
            const constraintData = state.issueData.filter(ele => ele.state === action.payload);
            if (action.payload === 'all') {
                return {
                    ...state,
                    passData: state.issueData
                }
            }
            else if (action.payload === 'open') {
                return {
                    ...state,
                    passData: constraintData,
                    open: constraintData.length,
                    close: state.issueData.length - constraintData.length,
                }
            }
            else {
                return {
                    ...state,
                    passData: constraintData,
                    open: state.issueData.length - constraintData.length,
                    closed: constraintData.length,
                }
            }

        case TITLE_SEARCH:
            if (action.payload !== '') {
                const options = {
                    shouldSort: true,
                    maxPatternLength: 15,
                    minMatchCharLength: 5,
                    keys: ['title'],
                    id: "id"
                };
                var fuse = new Fuse(state.issueData, options)
                const result = fuse.search(action.payload);
                const constraintData = state.issueData.filter(ele => result.includes(ele.id.toString()));
                const openIssues = constraintData.filter(ele => ele.state === 'open');
                const closedIssues = constraintData.filter(ele => ele.state === 'closed');
                return {
                    ...state,
                    passData: constraintData,
                    open: openIssues.length,
                    close: closedIssues.length
                }
            }
            else {
                const openIssues = state.issueData.filter(ele => ele.state === 'open');
                const closedIssues = state.issueData.filter(ele => ele.state === 'closed');
                return {
                    ...state,
                    passData: state.issueData,
                    open: openIssues.length,
                    close: closedIssues.length
                }
            }

        case ISSUES_SORT:
            let sortData = [];
            switch (action.payload) {
                case 'newest': sortData = state.passData.sort(function (a, b) { return (new Date(b.created_at).getTime() - new Date(a.created_at).getTime()) });
                    return {
                        ...state,
                        passData: sortData
                    }
                case 'oldest': sortData = state.passData.sort(function (a, b) { return (new Date(a.created_at).getTime() - new Date(b.created_at).getTime()) });
                    return {
                        ...state,
                        passData: sortData
                    }
                case 'recentlyUpdated': sortData = state.passData.sort(function (a, b) { return (new Date(b.updated_at).getTime() - new Date(a.updated_at).getTime()) });
                    return {
                        ...state,
                        passData: sortData
                    }
                case 'leastRecentlyUpdated': sortData = state.passData.sort(function (a, b) { return (new Date(a.updated_at).getTime() - new Date(b.updated_at).getTime()) });
                    return {
                        ...state,
                        passData: sortData
                    }
                default: break;
            }break;
        case LOGIN:
            return {
                ...state,
                login:action.payload
            }
        default:
            return state;
    }
}