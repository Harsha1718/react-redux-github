import React, { Component } from 'react';
import { BrowserRouter, Route} from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store/index';
import Issues from './components/issues/issues'
import comments from './components/comments/comments'
import isLogin from './components/login/homeHeader'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <BrowserRouter>
            <Route path="/" component={isLogin}/>
            <Route path="/issues" component={Issues} exact />
            <Route path="/issues/:number" component={comments} exact />
          </BrowserRouter>
        </div>
      </Provider>
    );
  }
}

export default App;
