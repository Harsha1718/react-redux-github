import React, { Component } from 'react';
import { connect } from 'react-redux'
import Header from './issuesComponents/header/headerContainer'
import './issues.css'
import { fetchIssues, filterAuthor, filterLabel, filterState, titleSearch, sortOptions, login } from '../../actions/issueActions'
import Pagination from './issuesComponents/issueBody/pagination'
// import BodyContainer from './issuesComponents/issueBody/bodyContainer'
// import Pagination from "./issuesComponents/pagination/pagination";


class Issues extends Component {

    addToSearch = (option, val) => {
        switch (option) {
            case "state": this.props.filterState(val);
                break;
            case "author": this.props.filterAuthor(val)
                break;
            case "label": this.props.filterLabel(val);
                break;
            case "titleSearch": this.props.titleSearch(val);
                break;
            case "sort": this.props.sortOptions(val);
                break;
            case "isLogin": this.props.login(val)
                break;
            default: break;
        }
    }


    componentDidMount = () => {
        // let page = parseInt(this.props.location.search.replace(/\D/g, ""));
        // (this.props.location.search) ? this.props.fetchIssues(page) : 
        this.props.fetchIssues(1);
    }
    handlePage = e => {
        // this.props.fetchIssues(e.selected + 1)
        this.props.history.push(`issues?page=${e.selected + 1}`)
    };

    render() {
        const data = this.props.issues;
        return (

            <div className="Issue">
                <div className='content'>
                    <Header addToSearch={this.addToSearch} open={data.open} closed={data.closed} all={data.open + data.closed} passData={data.passData} />
                    <br/>
                    <Pagination  data= {data.passData}></Pagination>
                    {/* <div>{data.passData.map(ele => <BodyContainer value={ele} />)}</div> */}
                    {/* <Pagination handlePage={this.handlePage.bind(this)} page={data.page - 1} totalPages={data.open + data.closed}/> */}
                </div>
            </div>

        );
    }
}

const mapStateToProps = state => ({
    issues: state.issues
})

export default connect(mapStateToProps, { fetchIssues, filterAuthor, filterLabel, filterState, titleSearch, sortOptions, login })(Issues);