import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import './pagination.css'
let pre = '';
let next = '';
class Pagination extends Component {
    let 
    render() {
        (this.props.page > 0 ? pre = 'previous' : pre = '');
        (this.props.page < this.props.totalPages / 30 -1? next = 'next': next = '')
        return (
            <ReactPaginate className="pages" previousLabel={pre} nextLabel={next} minRows={4} pageCount={this.props.totalPages/30} onPageChange={this.props.handlePage} activeClassName={"active-page"} containerClassName="pagination" forcePage={this.props.page}>
            </ReactPaginate>
        );
    }
}

export default Pagination;
