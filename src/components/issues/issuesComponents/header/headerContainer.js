import React, { Component } from 'react';
import './headerContainer.css'
// import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';

class Header extends Component {
    filter = (e) => {
        let query = e.target.className;
        if (query === "state") this.props.addToSearch(query, e.target.value)
        
        //label and author
        if (!e.nativeEvent.inputType) this.props.addToSearch(query, e.target.value)
        if (e.target.value === "") this.props.addToSearch(query, e.target.value)

        if (e.target.className === "searchTitle") this.props.addToSearch("titleSearch", e.target.value);
        if (e.target.className === "sort") this.props.addToSearch(query, e.target.value);
    }
    render() {
        const uniqueAuthor = this.props.passData.reduce((acc, ele) => {
            if (!acc.includes(ele.user.login)) {
                acc.push(ele.user.login)
            }
            return acc;
        }, []);
        const uniqueLabel = this.props.passData.reduce((acc, ele) => {
            ele.labels.forEach(label => { if (!acc.includes(label.name)) acc.push(label.name); })
            return acc;
        }, [])
        return (
            <div className="container">
                <input type="text" placeholder="Title Search" className="searchTitle" onChange={this.filter} />
                <div className='navBar'>
                    <button onClick={this.filter} className="state" value="all">{this.props.all} Total</button>
                    <button onClick={this.filter} className="state" value="open">{this.props.open} open</button>
                    <button onClick={this.filter} className="state" value="closed">{this.props.closed} closed</button>
                    <input className="label" list="labels" placeholder="Label" onChange={this.filter} />
                    <datalist id="labels">
                        {uniqueLabel.map((ele) => <option>{ele}</option>)}
                    </datalist>
                    <input className="author" list="author" placeholder="Author" onChange={this.filter} />
                    <datalist id="author">
                        {uniqueAuthor.map((ele) => <option>{ele}</option>)}
                    </datalist>
                    <select className="sort" onChange={this.filter}>
                        <option>newest</option>
                        <option>oldest</option>
                        <option>recentlyUpdated</option>
                        <option>leastRecentlyUpdated</option>
                    </select>
                </div>
            </div>
        );
    }
}

export default Header;