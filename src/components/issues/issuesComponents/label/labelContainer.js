import React, { Component } from 'react';
import './LabelContainer.css'
import tinycolor from 'tinycolor2';

class LabelContainer extends Component {
    render() {
        const labels = this.props.value;
        const buttonColor = '#' + this.props.color;
        const colorz = tinycolor(buttonColor);
        let textColor = "";
        colorz.isLight() ? textColor = 'black' : textColor = 'white';

        return (
            <button className="bt-style" style={{ backgroundColor: buttonColor, color: textColor}}>{labels}</button>
        );
    }
}

export default LabelContainer;