import React, { Component } from 'react';
import LabelContainer from '../label/labelContainer'
import './BodyContainer.css'
import Button from '@material-ui/core/Button';
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import List from '@material-ui/core/List';
import ErrorIcon from '@material-ui/icons/ErrorOutline'
import CommentIcon from '@material-ui/icons/Message'
import { Link } from "react-router-dom";

const styles = theme => ({
    openIcon: {
        color: "green"
    },
    closeIcon: {
        color: "red"
    },
    root: {
        backgroundColor: theme.palette.background.paper,
    },
})

class BodyContainer extends Component {
    render() {
        const { classes } = this.props
        const issues = this.props.value;
        const commentCount = () => {
            if (this.props.value.comments > 0) {
                return (
                    <div className="comments">
                        <CommentIcon />&nbsp;{issues.comments}
                    </div>
                )
            }
        }
        return (
                <List className={classes.root}>
                    <ListItem alignItems="flex-start" button>
                        <ListItemAvatar>
                            <ErrorIcon className={issues.state === 'open' ? classes.openIcon : classes.closeIcon} />
                        </ListItemAvatar>
                        <ListItemText
                            primary={
                                <React.Fragment>
                                    <Link to={`/issues/${issues.number}`}><Button style={{ float: "right" }}>{commentCount()}</Button>{issues.title}</Link>
                                    {issues.labels.map(ele => <LabelContainer color={ele.color} value={ele.name} />)}
                                </React.Fragment>
                            }
                            secondary={
                                <React.Fragment>
                                    #{issues.number} {issues.state} {moment(issues.created_at).fromNow()} by <a href={issues.user.url}>{issues.user.login}</a>
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                </List>
        )
    }
}

export default withStyles(styles)(BodyContainer);