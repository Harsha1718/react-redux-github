import React, { Component } from 'react';
import Input from '@material-ui/core/Input';
import './comments.css'
import moment from 'moment'
import { connect } from 'react-redux'
import MarkDown from 'react-markdown'
import { fetchComments, addComment, deleteComment } from '../../actions/commentActions'
import { access_token, userName } from '../../Authentication'

class Comments extends Component {

    componentDidMount = () => this.props.fetchComments(this.props.match.params.number);

    deleteComment = (e) => this.props.deleteComment(e.target.id, access_token);

    addComment = (e) => {
        if (e.key === 'Enter') {
            (access_token === null) ? alert("require login") : this.props.addComment(e.target.value, this.props.titleData.number, access_token);
            e.target.value = '';
        }
    }
    render() {
        let comments = this.props.comments, owner = this.props.owner, titleData = this.props.titleData;

        const deleteComment = (ele) => (ele.name === userName || owner === userName) ? (<button onClick={this.deleteComment} id={ele.id}>X</button>) : <span />
        const addComment = () => { if (access_token) return (<Input style={{ marginBottom: '2em', width: '50%' }} type="text" onKeyPress={this.addComment} placeholder="Add Comment" />) }
        const allComments = () => {
            if (comments.length) {
                let dumpData = comments.reduce((acc, ele) => {
                    acc.push({ name: ele.user.login, comment: ele.body, id: ele.id, updated_at: ele.updated_at })
                    return acc;
                }, [])
                return (
                    <div>
                        <div>{dumpData.map(ele =>
                            <div style={{ border: '1px solid black' }}> <span>{ele.name} : {ele.comment}</span>
                                <p style={{ color: "grey", fontSize: "12px" }}>commented {moment(ele.updated_at).fromNow()} <span style={{ float: 'right' }}>{deleteComment(ele)}</span></p>
                            </div>)}
                        </div>
                    </div>
                )
            }
            else {
                return [];
            }
        }
        if (titleData !== '') {
            return (
                <div>
                    <div className='comments'>
                        <div className="container">
                            <span className="headerTitle">{titleData.title}</span>
                            <span style={{ color: "grey" }}> {titleData.state} {moment(titleData.created_at).fromNow()} by {titleData.user.login}</span>
                            <MarkDown>{titleData.body}</MarkDown>
                            {allComments()}
                            <br />
                            {addComment()}
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return <div />
        }
    }
}

const mapStateToProps = state => ({
    comments: state.comments.comments,
    titleData: state.comments.titleData,
    owner: state.comments.owner

})

export default connect(mapStateToProps, { fetchComments, addComment, deleteComment })(Comments);