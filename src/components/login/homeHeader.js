import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'
import React, { Component } from 'react';
import { login } from '../../actions/commentActions'
import { Link } from "react-router-dom";
import './homeHeader.css'

class Header extends Component {
    isLogin=(e)=>{
        this.props.login(e.target.textContent)
    }

    render() {
        const data = this.props.issues;
        return (
            <div className="homeHeader">
                <Button onClick={this.isLogin}>{data.login}</Button> 
                <Link to='/issues'><Button>GIT-HUB</Button></Link>           
            </div>
        );
    }
}
const mapStateToProps = state => ({
    issues: state.issues
})

export default connect(mapStateToProps, { login })(Header);